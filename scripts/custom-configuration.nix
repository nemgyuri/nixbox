{ config, pkgs, ... }:

{
# Sample Postgres service
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_11;
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 11 ''
      local all all trust
      host all all ::1/128 trust
    '';
    initialScript = pkgs.writeText "backend-initScript" ''
      CREATE ROLE nixcloud WITH LOGIN PASSWORD 'nixcloud' CREATEDB;
      CREATE DATABASE nixcloud;
      GRANT ALL PRIVILEGES ON DATABASE nixcloud TO nixcloud;
    '';
  };

# Same with Tomcat
  services.tomcat = {
    enable = true;
    package = pkgs.tomcat9;
    jdk = pkgs.jre8_headless;
    javaOpts = "-Xms512m -Xmx1024m -XX:+UseG1GC";
  };

# Set static address for the "main" interface. Apparently it's not sufficient to do that in the Vagrantfile only as the NIC receives a link-local address in that case.
  networking.interfaces.enp0s8 = {
    ipv4.addresses = [{
      address = "10.0.0.40";
      prefixLength = 24;
    }];
  };

# Configure firewall to allow external connections towards Tomcat and Postgres, and SSH as well
  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 22 5432 8080 ];
    allowedUDPPorts = [ 22 5432 8080 ];
    logRefusedConnections = false;
  };

# Workaround to make Vagrant's shared folder always available and mounted
  fileSystems."/vagrant" = {
    fsType = "vboxsf";
    device = "vagrant";
    options = [ "rw" ];
  };

# Enable Podman
  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    networkSocket.openFirewall = false;
  };

}
