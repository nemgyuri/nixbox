NixOS boxes for Vagrant
=======================

[NixOS](http://nixos.org) is a linux distribution based on a purely functional package manager. This project builds [vagrant](http://vagrantup.com) .box and/or raw images.

**Important**: this repository is a fork or - in another words -, a spin-off of the [Nix Community's project of the same name](https://github.com/nix-community/nixbox).
The credit goes therefore to the folks contributing to that project. I only amended and corrected/updated some files a little bit, but the vast majority of the work was done by them.

Status
------

The project uses the latest stable version - NixOS 21.05.

Building the image
-------------------

As a prerequisite install [packer](http://packer.io) and [virtualbox](https://www.virtualbox.org/). The latter is only needed when you want to build and use a VirtualBox image.

Four packer builders are currently supported:
- Virtualbox
- qemu / libvirt
- VMware
- Hyper-V

Have a look at the different `make build` target to build your image.

**Caveat emptor**: The configuration uses a hard-coded IPv4 address for the main network interface of the resulting image. It's set to `10.0.0.40/24`. If that's not suitable for your needs, modify the following stanza in `scripts/custom-configuration.nix` (modify the value of "address"):
```
  networking.interfaces.enp0s8 = {
    ipv4.addresses = [{
      address = "10.0.0.40";
      prefixLength = 24;
    }];
  };
```

If you build on a host that does not support Makefile, here are some examples:
```
packer build --only=virtualbox-iso nixos-i686.json
packer build --only=qemu nixos-x86_64.json
packer build --only=vmware-iso nixos-x86_64.json
packer build --only=hyperv-iso nixos-x86_64.json
```

If you don't want a Vagrant box to be created in the end (i.e. you only want the raw image), invoke the build with the Packer binary and the "novagrant" JSON, e.g.
```
packer build --only=qemu nixos-x86_64-novagrant.json
```

Use the image
---------------------

The vagrant .box image or the raw image is now ready to go and you can use it.
Add the box in Vagrant as following:

```
vagrant box add --name nixbox32 packer_virtualbox-iso_virtualbox.box
# or
vagrant box add --name nixbox64 packer_virtualbox-iso_virtualbox.box
```

Automatic build
---------------
You can use the provided scripts `cleanup-virtualbox.sh` and `vagrant-build-virtualbox.sh` for automating image build, VM start and cleanup. These scripts are usable only when dealing with the **VirtualBox ISO** images!

Updating the ISO urls
---------------------

To update the ISO urls to the latest release run: `make update`

Troubleshooting
-----------------

If you build on a Windows OS, please make sure you keep the unix file encoding of the generated configuration files
(see [issue \#30](https://github.com/nix-community/nixbox/issues/30)

License
-------

Copyright 2015 under the MIT
