#!/bin/bash
# name: cleanup.sh
# Script for cleaning up when using Vagrant and Virtualbox

# variables, modify them if necessary
ARCH="x86_64"

# Destroy VM and its associated drives
vagrant destroy -f 

# Remove Vagrant box from the repository 
vagrant box remove nixos-21.05

# Delete build artefacts
rm -fv ./nixos-21.05-virtualbox-${ARCH}.box
