#!/bin/bash
# name: vagrant-build-virtualbox.sh
# Script for automating build actions when using Vagrant and Virtualbox

# Variables, modify them if necessary
VM_NAME="kalacskepu"
ARCH="x86_64"

if ! type -P packer &> /dev/null; then
    echo "Packer not found, exiting now."
    exit 1;
fi

# Build
packer build --only=virtualbox-iso nixos-${ARCH}.json

# Only if build was successful
if [[ -f ./nixos-21.05-virtualbox-${ARCH}.box ]]; then 
# Add vagrant box to the repository
    vagrant box add --name nixos-21.05 ./nixos-21.05-virtualbox-${ARCH}.box
# Start VM
    vagrant up
# Extract ssh config
    vagrant ssh-config > ./vagrant-ssh
# Inform user
    echo -e "Now you can login to the machine via \n ssh -F vagrant-ssh ${VM_NAME}"
fi
